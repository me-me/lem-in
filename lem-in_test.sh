#A basic script to test lem-in

#!bin/bash

# Declare colors
green="\033[32;7m"
cstop="\033[0m"
cwhite="\033[40;7m"
blue="\033[47;34m"
bblue="\033[44;7m"

# Recompile the library
 echo "$cwhite Compile library $cstop"
 make re
 echo "$bblue LIBRARY COMPILED <lem-in> CREATED$cstop\n"

# Define test
echo "$green choose a test: $cstop"
echo "$blue 1- None ants number $cstop"
echo "$blue 2- None start room $cstop"
echo "$blue 3- None end room $cstop"
echo "$blue 4- Add extra connected room $cstop"
echo "$blue 5- Add comments $cstop"
echo "$blue 6- Delete a connected room $cstop"
echo "$blue 7- Pass as an argumentgument a none existed file $cstop"
echo "$blue 8- Pass as argument an empty file $cstop"
echo "$blue 9- Delete test files $cstop\n"

# get input
echo "$green The number is: $cstop"
read answer

# treat the input
if [ $answer = "1" ]
then
	{
	echo "#none ants number
##start
0 1 2
##end
1 9 2
3 5 4
2 5 3
5 1 6
4 2 6
0-2
0-3
2-1
3-1
2-3
3-4
5-4" > test_1_None_ants
echo "$green FILE CREATED $cstop"
echo "$blue Show file: y/n $cstop"
	read s
	if [ $s = "y" ]
	then
		{
			echo "\n ======|| Start file ||========="
		cat test_1_None_ants
			echo "===========|| End file ||========="
		}
	fi
echo "$green Execute test with ./lem-in $cstop"
echo "$cwhite PRESS ENTER TO CONTINUE...$cstop"
read ENTER
	if [ $ENTER ]
	then
		echo ""
	fi
./lem-in < test_1_None_ants
echo "$green Do you want to try an other test? y/n $cstop"
read f
if [ $f = "y" ] 
then
	sh lem-in_test.sh
else
	echo "$cwhite Thank you for passing by...$cstop"
fi
} else if [ $answer = "2" ]
then 
	{
	echo "12
#None Start room
0 1 2
##end
1 9 2
3 5 4
2 5 3
5 1 6
4 2 6
0-2
0-3
2-1
3-1
2-3
3-4
5-4" > test_2_None_start
echo "$green FILE CREATED $cstop"
echo "$blue Show file: y/n $cstop"
	read s
	if [ $s = "y" ]
	then
		{
			echo "\n ======|| Start file ||========="
		cat test_2_None_start
			echo "===========|| End file ||========="
		}
	fi
echo "$green Execute test with ./lem-in $cstop"
echo "$cwhite PRESS ENTER TO CONTINUE...$cstop"
read ENTER
	if [ $ENTER ]
	then
		echo ""
	fi
./lem-in < test_2_None_start
echo "$green Do you want to try an other test? y/n $cstop"
read f
if [ $f = "y" ] 
then
	sh lem-in_test.sh
else
	echo "$cwhite Thank you for passing by...$cstop"
fi
}
else if [ $answer = "3" ]
then
	{
	echo "12
##start
0 1 2
#None_end_room
1 9 2
3 5 4
2 5 3
5 1 6
4 2 6
0-2
0-3
2-1
3-1
2-3
3-4
5-4" > test_3_None_end
echo "$green FILE CREATED $cstop"
echo "$blue Show file: y/n $cstop"
	read s
	if [ $s = "y" ]
	then
		{
			echo "\n ======|| Start file ||========="
		cat test_3_None_end
		echo "===========|| End file ||========="
		}
	fi
echo "$green Execute test with ./lem-in $cstop"
echo "$cwhite PRESS ENTER TO CONTINUE...$cstop"
read ENTER
	if [ $ENTER ]
	then
		echo ""
	fi
./lem-in < test_3_None_end
echo "$green Do you want to try an other test? y/n $cstop"
read f
if [ $f = "y" ] 
then
	sh lem-in_test.sh
else
	echo "$cwhite Thank you for passing by...$cstop"
fi
} else if [ $answer = "4" ]
then
	{
	echo "12
##start
0 1 2
##end
1 9 2
3 5 4
2 5 3
5 1 6
4 2 6
0-2
0-3
6-7
# Above is a none existed linked room
2-1
3-1
2-3
3-4
5-4" > test_4_none_link
echo "$green FILE CREATED $cstop"
echo "$blue Show file: y/n $cstop"
	read s
	if [ $s = "y" ]
	then
		{
			echo "\n ======|| Start file ||========="
		cat test_4_none_link
			echo "===========|| End file ||========="
		}
	fi
echo "$green Execute test with ./lem-in $cstop"
echo "$cwhite PRESS ENTER TO CONTINUE...$cstop"
read ENTER
	if [ $ENTER ]
	then
		echo ""
	fi
./lem-in < test_4_none_link
echo "$green Do you want to try an other test? y/n $cstop"
read f
if [ $f = "y" ] 
then
	sh lem-in_test.sh
else
	echo "$cwhite Thank you for passing by...$cstop"
fi
} else if [ $answer = "5" ]
then
	{
	echo "
13
##start
0 1 2
##end
1 9 2
#I am comment 01
3 5 4
2 5 3
#I am comment 02
5 1 6
4 2 6
0-2
0-3
2-1
# Hey there! YES i am a comment 03
3-1
2-3
3-4
5-4" > test_5_add_comment
echo "$green FILE CREATED $cstop"
echo "$blue Show file: y/n $cstop"
	read s
	if [ $s = "y" ]
	then
		{
			echo "\n ======|| Start file ||========="
		cat test_5_add_comment
			echo "===========|| End file ||========="
		}
	fi
echo "$green Execute test with ./lem-in $cstop"
echo "$cwhite PRESS ENTER TO CONTINUE...$cstop"
read ENTER
	if [ $ENTER ]
	then
		echo ""
	fi
./lem-in < test_5_add_comment
echo "$green Do you want to try an other test? y/n $cstop"
read f
if [ $f = "y" ] 
then
	sh lem-in_test.sh
else
	echo "$cwhite Thank you for passing by...$cstop"
fi
} else if [ $answer = "6" ]
then
	{
	echo "
12
##start
0 1 2
##end
# this is the deleted room 1 9 2
3 5 4
2 5 3
5 1 6
4 2 6
0-2
0-3
2-1
3-1
2-3
3-4
5-4" > test_6_del_room
echo "$green FILE CREATED $cstop"
echo "$blue Show file: y/n $cstop"
	read s
	if [ $s = "y" ]
	then
		{
			echo "\n ======|| Start file ||========="
		cat test_6_del_room
			echo "===========|| End file ||========="
		}
	fi
echo "$green Execute test with ./lem-in $cstop"
echo "$cwhite PRESS ENTER TO CONTINUE...$cstop"
read ENTER
	if [ $ENTER ]
	then
		echo ""
	fi
./lem-in < test_6_del_rooms
echo "$green Do you want to try an other test? y/n $cstop"
read f
if [ $f = "y" ] 
then
	sh lem-in_test.sh
else
	echo "$cwhite Thank you for passing by...$cstop"
fi
} else if [ $answer = "7" ]
then
	{
		echo "\033[32m Pass a none existed files! <shodaw_file>  $cstop"
	echo "$green Execute test with ./lem-in $cstop"
echo "$cwhite PRESS ENTER TO CONTINUE...$cstop"
read ENTER
	if [ $ENTER ]
	then
		echo ""
	fi
	./lem-in < shodaw_file
	echo "$green Do you want to try an other test? y/n $cstop"
read f
if [ $f = "y" ] 
then
	sh lem-in_test.sh
else
	echo "$cwhite Thank you for passing by...$cstop"
fi
} else if [ $answer = "8" ]
then
	{
		echo "\033[32mGenerate an empty file <empty-file> $cstop"
		echo " " > empty-file 
		echo "$green EMPTY FILE HAS BEEN CREATED $cstop"
	echo "$green Execute test with ./lem-in $cstop"
	echo "$cwhite PRESS ENTER TO CONTINUE...$cstop"
	read ENTER
	if [ $ENTER ]
	then
		echo ""
	fi
./lem-in < empty-file
echo "$green Do you want to try an other test? y/n $cstop"
read f
if [ $f = "y" ] 
then
	sh lem-in_test.sh
else
	echo "$cwhite Thank you for passing by...$cstop"
fi
} else if [ $answer = "9" ]
then
	{
		rm test_*
	}
fi
fi
fi
fi
fi
fi
fi
fi
fi
