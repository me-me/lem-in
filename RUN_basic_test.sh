
## check fichier auteur

echo "\033[32mVerify author file\033[0m"

echo "\033[40;7mPRESS ENTER TO CONTINUE...\033[0m"
read ENTER

if [ "ENTER" ]
then
	echo " "
fi

cat auteur > vartext

if [ "$vartext" == " " ]
then
printf'\033[i44;33;1mERROR: author file doesnt exist'
else
	{
	echo "\033[40;32mAuthor file exist:\033[0m"
	rm vartext
	 cat auteur			
	 echo "\033[40;32m[OK]\033[0m"
	}
fi

## check norminnette

echo "\n\033[40;32mChecking make file\033[0m"
echo "make"

## Break

echo "\033[40;7mPRESS ENTER TO CONTINUE...\033[0m"
read ENTER

if [ "ENTER" ]
then
	echo ""
fi

make

echo "\n\033[40;32mChecking make clean..\033[0m"


echo "\033[40;7mPRESS ENTER TO CONTINUE...\033[0m"
read ENTER

if [ "ENTER" ]
then
	echo ""
fi


make clean

echo "\nChecking make fclean..."

echo "\033[40;7mPRESS ENTER TO CONTINUE...\033[0m"
read ENTER

if [ "ENTER" ]
then
	echo ""
fi


make fclean


echo "\nChecking make re..."

echo "\033[40;7mPRESS ENTER TO CONTINUE...\033[0m"
read ENTER

if [ "ENTER" ]
then
	echo ""
fi

make re


echo "\nDo you want to fclean again? yes/no"
read varan
if [ "$varan" == "yes" ]
then
	{
		echo "make fclean again"
		make fclean
	}
fi

## Check norminnettie
echo "\nChecking norminette includes file..."

echo "\033[40;7mPRESS ENTER TO CONTINUE...\033[0m"
read ENTER

if [ "ENTER" ]
then
	echo ""
fi

norminette includes/


echo "\n\033[40;32mChecking norminette src files...\033[0m"

echo "\033[40;7mPRESS ENTER TO CONTINUE...\033[0m"
read ENTER

if [ "ENTER" ]
then
	echo ""
fi


norminette sources


echo "\n\033[40;32mChecking norminette libft...\033[0m"

echo "\033[40;7mPRESS ENTER TO CONTINUE...\033[0m"
read ENTER

if [ "ENTER" ]
then
	echo ""
fi

norminette libft/libft/

## Break

echo "\033[40;7mPRESS ENTER TO CONTINUE...\033[0m"
read ENTER

if [ "ENTER" ]
then
	echo "\033[40;32mEND OF BASIC TESTS!\n\033[0m"
fi

## end
