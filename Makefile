# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/08/03 16:11:59 by abnaceur          #+#    #+#              #
#    Updated: 2017/08/07 20:26:30 by abnaceur         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #


NAME= lem-in

CC= gcc

CFLAGS= -Wall -Werror -Wextra

SRC_NAME= main.c error.c read.c read_utils.c overview.c block.c waiting_line.c\
		  ant_move.c utils.c

SRC_PATH= ./sources/

INC_PATH= ./includes/

LIB_NAME= libft.a

LIB_ID= ft

LIB_PATH= ./libft/

LIB_INC= ./libft/includes/

OBJ_NAME= $(SRC_NAME:.c=.o)

OBJ_PATH= ./obj/

SRC= $(addprefix $(SRC_PATH), $(SRC_NAME))

OBJ= $(addprefix $(OBJ_PATH), $(OBJ_NAME))

LIB= $(addprefix $(LIB_PATH), $(LIB_NAME))

.PHONY: all
all: $(NAME)

$(NAME): $(LIB) $(OBJ)
	@$(CC) $(CFLAGS) -L$(LIB_PATH) -I$(INC_PATH) -o $(NAME) -l$(LIB_ID) $(OBJ)
	@echo "\033[42;30mLIBRARY CREATED"

$(LIB):
	@make re -C $(LIB_PATH)

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	@mkdir $(OBJ_PATH) 2> /dev/null || echo "" > /dev/null
	@$(CC) -I$(INC_PATH) -I$(LIB_INC) $(CFLAGS) -o $@ -c $<

.PHONY: clean
clean:
	@rm -fv $(OBJ)
	@rm -rf $(OBJ_PATH)
	@make $@ -C $(LIB_PATH)
	@echo "\033[42;30mLIBRARY CLEANED"

.PHONY: fclean
fclean: clean
	@rm -fv $(NAME)
	@make $@ -C $(LIB_PATH)
	@echo "\033[42;30mLIBRARY FULL CLEANED\033[0m"

.PHONY: re
re: fclean all
