/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 23:10:33 by abnaceur          #+#    #+#             */
/*   Updated: 2017/08/03 23:13:04 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	exit_error(char *str)
{
	if (str != NULL)
		ft_printf_fd(1, "ERROR : %s\n", str);
	else
		ft_printf_fd(1, "ERROR\n");
	exit(1);
}

void	*secure_alloc(size_t size)
{
	void *result;

	result = ft_memalloc(size);
	if (result == NULL)
		exit_error(strerror(errno));
	return (result);
}
