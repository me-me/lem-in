/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 22:23:44 by abnaceur          #+#    #+#             */
/*   Updated: 2017/08/07 20:47:04 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int	main(void)
{
	t_anthill	*anthill;

	anthill = secure_alloc(sizeof(t_anthill));
	read_anthill(0, anthill);
	print_overview(anthill->list_line);
	overview(anthill);
	ft_putendl("");
	while (anthill->finish_ants < anthill->number_ants)
	{
		one_turn(anthill);
	}
	return (0);
}
