# A simple script to execute Lem-in visulalizer
#!bin/bash

# recompile Library

make re ./visualizer_lem-in/libft/ 

# CREATE EXECUTABLE FOR VISUALIZER
echo "\033[40;32mCREATE VISUALIZER\033[0m"

cp ./visualizer_lem-in/mystyle.css ./
cp ./visualizer_lem-in/Lem-in_webBased_visualizer ./ 

echo "\033[40;32mEXECUTABLE HAS BEEN CREATED!\033[0m"

# import main visulazer
echo "\033[47;32m visualizer.c has been imported\033[0m"
cp /Users/abnaceur/execute/visualizer_lem-in/main.c ./ 
mv main.c visualizer.c

#CREAT AN SH FOR CLEANING
echo "\033[40;7mA CLEANING FILE WILL BE CREATED <remove.sh>"

echo "
		rm Lem-in_webBased_visualizer mystyle.css\n
		rm remove.sh\n
		make fclean\n
		rm Web-page.html\n
		rm myscript.js\n
		rm RUN.sh\n
		rm visualizer.c" > remove.sh

		echo "\033[40;7mA CLEANING FILES HAS BEEN CREATED <remove.sh>"

		
		echo "echo \"\033[40;32mChoose an option:\033[0m\"
	echo \" 1- Use valid_test file \"
	echo \" 2- Use your own file \"
	read d
	if [ \$d = \"1\" ]
	then
./lem-in < valid_test | ./Lem-in_webBased_visualizer
else
	{
		echo \"Enter a valid File path:\"
read c
./lem-in < \$c | ./Lem-in_webBased_visualizer 
}
fi" > RUN.sh
echo "\033[40;32m| sh RUN.sh |-->  to start the visualizer!\033[0m"

		echo "\033[40;7mPRESS ENTER TO CONTINUE\033[0m"
		read ENTER
		if [ ENTER ]
		then
			echo ""
		fi
echo "\033[41;32mSee visualizer source code: y/n\033[0m"
read f
if [ $f = "y" ]
then 
	vim visualizer.c
fi

