/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_str.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 22:40:36 by abnaceur          #+#    #+#             */
/*   Updated: 2017/08/03 22:40:59 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	print_str(char *str, t_value *v, int fd)
{
	ft_putstr_fd(str, fd);
	if (v->char_null)
		ft_putchar_fd(*str, fd);
}

void	wprint_str(US *str, t_value *v, int fd, int size)
{
	ft_putwstr_size_fd(str, fd, size);
	if (v->char_null)
		ft_putwchar_fd(*str, fd);
}
