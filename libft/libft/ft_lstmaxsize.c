/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmaxsize.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 22:25:01 by abnaceur          #+#    #+#             */
/*   Updated: 2017/08/03 22:31:40 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_lstmaxsize(t_list *lst)
{
	size_t size_next;

	if (lst == NULL)
		return (0);
	size_next = ft_lstmaxsize(lst->next);
	return (lst->content_size > size_next ? lst->content_size : size_next);
}
