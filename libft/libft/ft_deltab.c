/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_deltab.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 22:21:57 by abnaceur          #+#    #+#             */
/*   Updated: 2017/08/03 22:31:09 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_deltab(void *ad, size_t size)
{
	size_t	i;
	void	**tab;

	if (ad == NULL)
		return ;
	tab = (void**)ad;
	i = 0;
	while (i < size)
	{
		if (tab[i] != NULL)
			ft_memdel(&tab[i]);
		i++;
	}
	ft_memdel((void**)&tab);
}
