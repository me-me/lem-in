/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_readstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 22:28:41 by abnaceur          #+#    #+#             */
/*   Updated: 2017/08/03 22:33:02 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <unistd.h>

int	ft_readstr(char *str)
{
	char	c;
	int		i;
	int		result;

	c = 0;
	i = 0;
	result = 1;
	while (c != '\n')
	{
		read(0, &c, 1);
		if (c == '\n' && str[i])
			result = 0;
		if (c != '\n' && result == 1 && str[i] != c)
			result = 0;
		i++;
	}
	if (i == 1)
		return (-1);
	return (result);
}
