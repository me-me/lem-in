/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 22:24:43 by abnaceur          #+#    #+#             */
/*   Updated: 2017/08/03 22:31:35 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

void	ft_lstdelin(t_list **alst, t_list *elem, void (*del)(void*, size_t))
{
	t_list *before;

	if ((alst == NULL || *alst == NULL) || elem == NULL)
		return ;
	if (*alst == elem)
		*alst = elem->next;
	else
	{
		before = ft_lstbefore(*alst, elem);
		if (before != NULL)
			before->next = elem->next;
	}
	if (elem->content != NULL)
		del(elem->content, elem->content_size);
	free(elem);
}

void	ft_lstdelin_memdel(t_list **alst, t_list *elem)
{
	t_list *before;

	if ((alst == NULL || *alst == NULL) || elem == NULL)
		return ;
	if (*alst == elem)
		*alst = elem->next;
	else
	{
		before = ft_lstbefore(*alst, elem);
		if (before != NULL)
			before->next = elem->next;
	}
	if (elem->content != NULL)
		ft_memdel(&(elem->content));
	free(elem);
}
