/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstfind.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 22:24:50 by abnaceur          #+#    #+#             */
/*   Updated: 2017/08/03 22:31:37 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstfind(t_list *lst, void *content, int (*cmp)(void *c1, void *c2))
{
	if (lst == NULL || cmp == NULL)
		return (NULL);
	if (cmp(content, lst->content))
		return (lst);
	return (ft_lstfind(lst->next, content, cmp));
}
