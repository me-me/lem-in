/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_before.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 22:24:24 by abnaceur          #+#    #+#             */
/*   Updated: 2017/08/03 22:31:29 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd_before(t_list **alst, t_list *old, t_list *new)
{
	t_list *temp;

	if ((alst == NULL || *alst == NULL) || new == NULL || old == NULL)
		return ;
	if (*alst == old)
	{
		ft_lstadd(alst, new);
		return ;
	}
	temp = ft_lstbefore(*alst, old);
	if (temp == NULL)
		return ;
	new->next = old;
	temp->next = new;
}
