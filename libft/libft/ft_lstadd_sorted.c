/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_sorted.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 22:24:32 by abnaceur          #+#    #+#             */
/*   Updated: 2017/08/03 22:31:31 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd_sorted(t_list **alst, t_list *elem,\
		int (*cmp)(void *c1, void *c2))
{
	if (alst == NULL || elem == NULL || cmp == NULL)
		return ;
	if (*alst == NULL)
	{
		ft_lstadd(alst, elem);
		return ;
	}
	if (cmp(elem->content, (*alst)->content) <= 0)
		ft_lstadd_before(alst, *alst, elem);
	else if ((*alst)->next == NULL)
		(*alst)->next = elem;
	else
		ft_lstadd_sorted(&((*alst)->next), elem, cmp);
}

void	ft_lstadd_sorted_o(t_list **alst, t_list *elem, void *option,\
		int (*cmp)(void *c1, void *c2, void *opt))
{
	if (alst == NULL || elem == NULL || cmp == NULL)
		return ;
	if (*alst == NULL)
	{
		ft_lstadd(alst, elem);
		return ;
	}
	if (cmp(elem->content, (*alst)->content, option) <= 0)
		ft_lstadd_before(alst, *alst, elem);
	else if ((*alst)->next == NULL)
		(*alst)->next = elem;
	else
		ft_lstadd_sorted_o(&((*alst)->next), elem, option, cmp);
}
