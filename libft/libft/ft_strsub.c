/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 22:30:06 by abnaceur          #+#    #+#             */
/*   Updated: 2017/08/03 22:38:25 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char			*result;
	unsigned int	i;

	if (s == NULL)
		return (NULL);
	result = ft_strnew(len + 1);
	if (result == NULL)
		return (NULL);
	i = 0;
	while (i < (unsigned int)len)
	{
		result[i] = s[start + i];
		i++;
	}
	return (result);
}
