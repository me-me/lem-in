/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstsort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 22:27:18 by abnaceur          #+#    #+#             */
/*   Updated: 2017/08/03 22:31:43 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_lstsort_step(t_list **begin, t_list *before, t_list *elem,\
				int (*cmp)(void *c1, void *c2))
{
	t_list *temp;

	if (begin == NULL || *begin == NULL || elem == NULL || elem->next == NULL)
		return ;
	temp = elem;
	if (cmp(elem->content, elem->next->content) > 0)
	{
		temp = elem->next;
		elem->next = temp->next;
		temp->next = elem;
		if (before != NULL)
		{
			before->next = temp;
			ft_lstsort_step(begin, ft_lstbefore(*begin, before), before, cmp);
		}
		else
			*begin = temp;
		ft_lstsort_step(begin, temp, elem, cmp);
	}
	else
		ft_lstsort_step(begin, elem, elem->next, cmp);
}

void		ft_lstsort(t_list **alst, int (*cmp)(void *c1, void *c2))
{
	ft_lstsort_step(alst, NULL, *alst, cmp);
}
