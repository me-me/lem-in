/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_random.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 22:28:39 by abnaceur          #+#    #+#             */
/*   Updated: 2017/08/03 22:33:01 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>
#include <time.h>

int		ft_random(int min, int max)
{
	int			rn;
	static int	first = 1;

	if (first)
	{
		first = 0;
		srand(time(NULL));
	}
	rn = min + rand() % (max - min + 1);
	return (rn);
}
