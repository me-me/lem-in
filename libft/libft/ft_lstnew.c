/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 22:25:05 by abnaceur          #+#    #+#             */
/*   Updated: 2017/08/03 22:31:41 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list *result;

	result = (t_list*)ft_memalloc(sizeof(t_list));
	if (result == NULL)
		return (NULL);
	result->next = NULL;
	if (content == NULL)
	{
		result->content = NULL;
		result->content_size = 0;
		return (result);
	}
	result->content_size = content_size;
	result->content = ft_memalloc(content_size);
	if (result->content == NULL)
	{
		result->content_size = 0;
		return (NULL);
	}
	else
		ft_memcpy(result->content, content, content_size);
	return (result);
}

t_list	*ft_lstnew_noalloc(void *content, size_t content_size)
{
	t_list *result;

	result = (t_list*)ft_memalloc(sizeof(t_list));
	if (result == NULL)
		return (NULL);
	result->next = NULL;
	if (content == NULL)
	{
		result->content = NULL;
		result->content_size = 0;
		return (result);
	}
	result->content_size = content_size;
	result->content = content;
	if (result->content == NULL)
	{
		result->content_size = 0;
		return (NULL);
	}
	return (result);
}
