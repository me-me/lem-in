/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   significant_bit.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 22:30:47 by abnaceur          #+#    #+#             */
/*   Updated: 2017/08/03 22:30:54 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	significant_bit(void *p, size_t size)
{
	int				result;
	unsigned char	mask;
	size_t			i;
	int				j;
	unsigned char	*ad;

	i = 0;
	result = 0;
	ad = p + size - 1;
	while (i < size)
	{
		mask = 0x80;
		j = -1;
		while (++j < 8)
		{
			if (!(mask & *ad))
				result++;
			else
				return (size * 8 - result);
			mask = mask >> 1;
		}
		i++;
		ad--;
	}
	return (0);
}
