/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 22:27:39 by abnaceur          #+#    #+#             */
/*   Updated: 2017/08/03 22:31:48 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	size_t			i;
	unsigned char	c_temp;
	unsigned char	*s_temp;

	i = 0;
	c_temp = (unsigned char)c;
	s_temp = (unsigned char*)s;
	while (i < n)
	{
		if (c_temp == s_temp[i])
			return (&(s_temp[i]));
		i++;
	}
	return (NULL);
}
