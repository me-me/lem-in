/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 22:27:37 by abnaceur          #+#    #+#             */
/*   Updated: 2017/08/03 22:31:47 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, void *src, int c, size_t n)
{
	size_t				i;
	unsigned char		*src_temp;
	unsigned char		*dst_temp;

	i = 0;
	src_temp = (unsigned char*)src;
	dst_temp = (unsigned char*)dst;
	while (i < n)
	{
		dst_temp[i] = src_temp[i];
		if (src_temp[i] == (unsigned char)c)
			return (&(dst_temp[i + 1]));
		i++;
	}
	return (NULL);
}
