/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nbrcmp.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 22:30:42 by abnaceur          #+#    #+#             */
/*   Updated: 2017/08/03 22:38:30 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	min_of(int n, int *tab)
{
	int		result;
	int		tmp;
	int		i;

	i = 0;
	while (i < n)
	{
		tmp = tab[i];
		if (i == 0)
			result = tmp;
		else if (tmp < result)
			result = tmp;
		i++;
	}
	return (result);
}

int	max_of(int n, int *tab)
{
	int		result;
	int		tmp;
	int		i;

	i = 0;
	while (i < n)
	{
		tmp = tab[i];
		if (i == 0)
			result = tmp;
		else if (tmp > result)
			result = tmp;
		i++;
	}
	return (result);
}
