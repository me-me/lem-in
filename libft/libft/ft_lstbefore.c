/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstbefore.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 22:24:35 by abnaceur          #+#    #+#             */
/*   Updated: 2017/08/03 22:31:33 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstbefore(t_list *lst, t_list *elem)
{
	if (lst == elem)
		return (NULL);
	while (lst->next != elem && lst->next != NULL)
		lst = lst->next;
	if (lst->next == NULL)
		return (NULL);
	else
		return (lst);
}
