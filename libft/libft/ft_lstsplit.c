/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 22:27:21 by abnaceur          #+#    #+#             */
/*   Updated: 2017/08/03 22:31:45 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstsplit(char const *s, char c)
{
	char	**tab_temp;
	t_list	*result;
	t_list	*temp;
	int		i;

	tab_temp = ft_strsplit(s, c);
	if (tab_temp == NULL)
		return (NULL);
	i = 0;
	result = NULL;
	while (tab_temp[i] != NULL)
	{
		temp = ft_lstnew(tab_temp[i], ft_strlen(tab_temp[i]) + 1);
		ft_lstadd_end(&result, temp);
		ft_strdel(&(tab_temp[i]));
		i++;
	}
	ft_memdel((void**)&tab_temp);
	return (result);
}
